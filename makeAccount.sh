#!/bin/bash
#
#	Creates an account at Mullvad.net
#	Author: Simon Cedergren
#	Date 09 september 2014
#
ACCOUNT_HTML="account.html.tmp"
ACCOUNTFILE="accountFile.tmp"
ACCOUNTNUMBER="accountnumber.txt"
COOKIE="cookie"


# Check if running as root
if [ "$(whoami)" != 'root' ]; then
	echo "Connecting to OpenVPN requires root."
	exit 1;
fi

#Repeat!
while [ true ]; do

# Create the account and save the whole result to account.html. TODO: Add safety that retries until new account is created
curl --data "create_account=create" --cookie "language=sv" https://mullvad.net/en/account/ > $ACCOUNT_HTML

# Extract the specific row where the account number should be located
awk "NR==93" $ACCOUNT_HTML > $ACCOUNTFILE

# Extract the account number
cat $ACCOUNTFILE | grep -Eo '[0-9]{0,12}' > $ACCOUNTNUMBER

# Give $ACCOUNT the value of the account number
ACCOUNT=$(cat $ACCOUNTNUMBER)

if [ -z "$ACCOUNT" ]; then
	exit 1;
fi

# Log in with the new account to retrive the config files, save the cookie
LOGIN_FILE="login.html"
wget --save-cookies $COOKIE --post-data "account=$ACCOUNT" https://mullvad.net/en/setup/openvpn/ -O $LOGIN_FILE

# Download the (swedish) config file
wget --load-cookies $COOKIE https://mullvad.net/en/config/?server=se

# Change name of the config file, since it doesn't make any sense
mv index.html?server=se mullvad.zip

# Extract the configs and remove it
unzip -o mullvad.zip
rm mullvad.zip

# Create the installation directory
mkdir -p /etc/openvpn/mullvad/

# Copy the config to the OpenVPN directory
if [ -z "$ACCOUNT" ]; then
	exit 1;
fi
cp -R $ACCOUNT/* /etc/openvpn/mullvad/

# Set the config permissions to 400
chmod 400 /etc/openvpn/mullvad/*
chmod +x /etc/openvpn/mullvad/*


# Connect to the new VPN-config
cd /etc/openvpn/mullvad/ 
openvpn mullvad_linux.conf &
cd -

# Sleep for 2h and 50min
sleep 170m
echo "New config file will be configured in 3..."
sleep 1
echo "2..."
sleep 1
echo "1..."
sleep 1
echo "Will now fetch a new config!"

# Clean up the files
rm $ACCOUNTFILE
rm $ACCOUNT_HTML
rm $COOKIE
rm $LOGIN_FILE
rm $ACCOUNTNUMBER
rm -r $ACCOUNT

done
